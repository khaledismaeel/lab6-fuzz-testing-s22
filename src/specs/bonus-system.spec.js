import { calculateBonuses } from "../bonus-system";

const sampleAmount1 = 0;

it('chooses the correct multiplier', () => {
  expect(calculateBonuses('Standard', sampleAmount1)).toBeCloseTo(0.05);
  expect(calculateBonuses('Premium', sampleAmount1)).toBeCloseTo(0.1);
  expect(calculateBonuses('Diamond', sampleAmount1)).toBeCloseTo(0.2);
});

it('correctly handles invalid programs', () => {
  expect(calculateBonuses('sibelius crashed', sampleAmount1)).toBeCloseTo(0.0);
});

it('chooses the correct bonus', () => {
  const program = 'Standard';
  const multiplier = 0.05;
  
  const lim = [10000, 50000, 100000];
  const bonus = [1, 1.5, 2, 2.5];

  for (let i = 0; i < lim.length; ++i) {
    expect(calculateBonuses(program, lim[i] - 1)).toBeCloseTo(multiplier * bonus[i]);
    expect(calculateBonuses(program, lim[i])).toBeCloseTo(multiplier * bonus[i + 1]);
  }
});
